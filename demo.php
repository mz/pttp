<?php
/**
 * PTTP : the mvc framework for workerman & mzphp
 * run this file:
 * run demo.php [port] [online|debug]
 * example:
 * run demo.php 80 debug
 */
// defined mzphp framework path
define('FRAMEWORK_PATH', dirname(__FILE__) . '/mzphp/');
// deinfed worker
define('IN_WORKERMAN', 1);
// open debug
define('DEBUG', 0);
//error_reporting(E_ERROR);

// auto loader for framework
include 'Autoloader.php';

// worker count
$work_count = 10;
// port of web server
$root_port = isset($argv[2]) ? (int)$argv[2] : 8888;
// application path same as file name
$root_dir = substr(__FILE__, strlen(__DIR__) + 1, -4);
// reset argc 
$argc = 1;
// build pptp server object
$worker = new \pttp\PttpServer('http://0.0.0.0:' . $root_port, array('env' => (isset($argv[3]) ? $argv[3] : 'online')));
// worker 数量
$worker->count = $work_count;
// add root for server
$worker->addRoot('*', './app' . DIRECTORY_SEPARATOR . $root_dir . DIRECTORY_SEPARATOR);
\Workerman\Worker::$stdoutFile = 'run.log';
unset($argv[2], $argv[3]);
// run
//$worker->run();
\Workerman\Worker::runAll();
?>