<?php

!defined('FRAMEWORK_PATH') && exit('Access Deined.');

class index_control extends base_control {

    function __construct(&$conf) {
        parent::__construct($conf);
    }

    public function on_index() {

        $username = 'Jobs';
        VI::assign('username', $username);
        $this->show('index.htm');

    }
}

?>