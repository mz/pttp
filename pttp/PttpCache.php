<?php

namespace pttp;
/**
 * Class PttpCache LRU内存缓存
 */
class PttpCache {

    // object Node representing the head of the list
    private $head;

    // object Node representing the tail of the list
    private $tail;

    // int the max number of elements the cache supports
    private $capacity;

    // Array representing a naive hashmap
    private $hashmap;

    // max size of memory
    private $max_size = 0;

    // current size of memory
    private $memory = 0;

    /**
     *
     * @param int $capacity the max number of elements the cache allows
     * @param int $max_size the max size of elements memory
     */
    public function __construct($capacity, $max_size = 1024000) {
        $this->capacity = $capacity;
        $this->max_size = $max_size;
        $this->memory = 0;
        $this->hashmap = array();
        $this->head = new Node(null, null, 0);
        $this->tail = new Node(null, null, 0);
        $this->head->setNext($this->tail);
        $this->tail->setPrevious($this->head);
    }

    /**
     * Get an element with the given key
     *
     * @param string $key the key of the element to be retrieved
     * @return mixed the content of the element to be retrieved
     */
    public function get($key) {

        if (!isset($this->hashmap[$key])) {
            return null;
        }

        $node = $this->hashmap[$key];
        if (count($this->hashmap) == 1) {
            return $node->getData();
        }

        // refresh the access
        $this->detach($node);
        $this->attach($this->head, $node);

        return $node->getData();
    }

    /**
     * Inserts a new element into the cache
     *
     * @param string $key  the key of the new element
     * @param string $data the content of the new element
     * @return boolean true on success, false if cache has zero capacity
     */
    public function put($key, $data) {
        if ($this->capacity <= 0) {
            return false;
        }
        $size = isset($data['size']) ? $data['size'] : strlen($data);
        while ($this->memory + $size > $this->max_size) {
            $nodeToRemove = $this->tail->getPrevious();
            $this->detach($nodeToRemove);
            unset($this->hashmap[$nodeToRemove->getKey()]);
        }

        if (isset($this->hashmap[$key]) && !empty($this->hashmap[$key])) {
            $node = $this->hashmap[$key];
            // insert top of hashmap
            $this->detach($node);
            $this->attach($this->head, $node);
            // update data
            $node->setData($data);
            $node->setSize($size);
        } else {
            $node = new Node($key, $data, $size);
            $this->hashmap[$key] = $node;
            $this->attach($this->head, $node);

            // check if cache is full
            if (count($this->hashmap) > $this->capacity) {
                // we're full, remove the tail
                $nodeToRemove = $this->tail->getPrevious();
                $this->detach($nodeToRemove);
                unset($this->hashmap[$nodeToRemove->getKey()]);
            }
        }
        return true;
    }

    /**
     * Removes a key from the cache
     *
     * @param string $key key to remove
     * @return bool true if removed, false if not found
     */
    public function remove($key) {
        if (!isset($this->hashmap[$key])) {
            return false;
        }
        $nodeToRemove = $this->hashmap[$key];
        $this->detach($nodeToRemove);
        unset($this->hashmap[$nodeToRemove->getKey()]);
        return true;
    }

    /**
     * Adds a node to the head of the list
     *
     * @param Node $head the node object that represents the head of the list
     * @param Node $node the node to move to the head of the list
     */
    private function attach($head, $node) {
        $node->setPrevious($head);
        $node->setNext($head->getNext());
        $node->getNext()->setPrevious($node);
        $node->getPrevious()->setNext($node);
        $this->memory += $node->getSize();
    }

    /**
     * Removes a node from the list
     *
     * @param Node $node the node to remove from the list
     */
    private function detach($node) {
        $this->memory -= $node->getSize();
        $node->getPrevious()->setNext($node->getNext());
        $node->getNext()->setPrevious($node->getPrevious());
    }

}

/**
 * Class that represents a node in a doubly linked list
 */
class Node {

    /**
     * the key of the node, this might seem reduntant,
     * but without this duplication, we don't have a fast way
     * to retrieve the key of a node when we wan't to remove it
     * from the hashmap.
     */
    private $key;

    private $length;

    // the content of the node
    private $data;

    // the next node
    private $next;
    // length of data
    private $size;

    // the previous node
    private $previous;

    /**
     * @param string $key  the key of the node
     * @param string $data the content of the node
     * @param string $size the content length of node
     */
    public function __construct($key, $data, $size) {
        $this->key = $key;
        $this->data = $data;
        $this->size = $size;
    }


    /**
     * @return mixed
     */
    public function getSize() {
        return $this->size;
    }

    /**
     * @param mixed $len
     */
    public function setSize($size) {
        $this->size = $size;
    }

    /**
     * Sets a new value for the node data
     *
     * @param string the new content of the node
     */
    public function setData($data) {
        $this->data = $data;
    }

    /**
     * Sets a node as the next node
     *
     * @param Node $next the next node
     */
    public function setNext($next) {
        $this->next = $next;
    }

    /**
     * Sets a node as the previous node
     *
     * @param Node $previous the previous node
     */
    public function setPrevious($previous) {
        $this->previous = $previous;
    }

    /**
     * Returns the node key
     *
     * @return string the key of the node
     */
    public function getKey() {
        return $this->key;
    }

    /**
     * Returns the node data
     *
     * @return mixed the content of the node
     */
    public function getData() {
        return $this->data;
    }

    /**
     * Returns the next node
     *
     * @return Node the next node of the node
     */
    public function getNext() {
        return $this->next;
    }

    /**
     * Returns the previous node
     *
     * @return Node the previous node of the node
     */
    public function getPrevious() {
        return $this->previous;
    }

}

?>